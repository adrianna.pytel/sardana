.. currentmodule:: sardana.pool

.. _sardana-triggergate-overview:

=======================
Trigger/gate overview
=======================

The trigger/gate represents synchronization devices like for example the
digital trigger and/or gate generators. Their main role is to synchronize
acquisition of the experimental channels.

Trigger or gate characteristics could be described in either the time and/or
the position configuration domains.

.. seealso::

    :ref:`sardana-triggergate-api`
        the trigger/gate :term:`API` 

    :class:`~sardana.tango.pool.TriggerGate.TriggerGate`
        the trigger/gate tango device :term:`API`
